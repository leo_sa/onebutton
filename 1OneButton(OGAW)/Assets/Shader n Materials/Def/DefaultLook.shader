Shader "Unlit/DefaultLook"
{
	Properties
	{
		_Color("Color", color) = (1, 1, 1, 1)
		_Intensity("Outline Intensity", Range(0, 4)) = 1
		_Thickness("Outline Thickness", Range(0, 1)) = 0.5
		_OutlineDivider("Outline division", Range(1, 10)) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
		//source https://docs.unity3d.com/Packages/com.unity.shadergraph@6.9/manual/Fresnel-Effect-Node.html
		void Fresnel(float3 normal, float3 viewDir, float intensity, out float Out) {
			Out = pow((1.0 - saturate(dot(normalize(normal), normalize(viewDir)))), intensity);
		}



            struct appdata
            {
				float3 normal : NORMAL;
                float4 vertex : POSITION;
                float3 view : TEXCOORD0;
            };

            struct v2f
            {
				float3 normal : NORMAL;
                float3 view : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.view = normalize(WorldSpaceViewDir(v.vertex));
				o.normal = v.normal;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

			float4 _Color;
			float _Intensity;
			float _Thickness;
			float _OutlineDivider;
            fixed4 frag (v2f i) : SV_Target
            {
				float4 outlineColor = _Color / _OutlineDivider;

                // sample the texture
                fixed4 col = _Color;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				float t = 0;
				Fresnel(i.normal, i.view, _Intensity, t);
				t = step(t, _Thickness);
				fixed4 final = 0;
				final = lerp(outlineColor, col, t);

                return final;
            }
            ENDHLSL
        }
    }
}